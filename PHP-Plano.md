# PymeFlow: Implementación con PHP Plano

## Instalación
```sh
composer require sebacarrasco93/pymeflow
```

### Crear una orden (create.php)
```php
// 1 Importar clases
use SebaCarrasco93\PymeFlow\Config;
use SebaCarrasco93\PymeFlow\PymeFlow;

// 2 Inicializar clase con parámetros de configuración
$config = new Config('api_key', 'secret_key', 'api_url', 'base_url');

// 3 Inicializar la API con los datos y pasar la clase de configuración (del paso anterior) como parámetro
$pymeflow = new PymeFlow($config);

// 4 Usar!
$order = rand(1000, 2000);
$subject = 'Pago de prueba';
$amount = 5000;
$email = 'cliente@dominio.tld';
$paymentMethod = 9;
$optional = ['rut' => '9999999-9', 'otroDato' => 'otroDato'];

$pymeflow->create($order, $subject, $amount, $email, $paymentMethod, $optional);
```

### Obtener el resultado (result.php)
```php
// 1 Importar clases
use SebaCarrasco93\PymeFlow\Config;
use SebaCarrasco93\PymeFlow\PymeFlow;

// 2 Inicializar clase con parámetros de configuración
$config = new Config('api_key', 'secret_key', 'api_url', 'base_url');

// 3 Inicializar la API con los datos y pasar la clase de configuración (del paso anterior) como parámetro
$pymeflow = new PymeFlow($config);

// Obtener el token enviado por POST
$token = $_POST['token'];

// Obtener resultado desde Flow (con detalle)
$result = $pymeflow->result($token);
// return $result;

// Actualizar la información en la DB, mostrar el Voucher propio, etc...
// ...

// O también sólo obtener el estado en formato de texto
$status = $pymeflow->status($token);
// return $status;

// O incluso, saber si fue pagada (devuelve true o false)
$isPaid = $pymeflow->isPaid($token);
// return $isPaid;
```
