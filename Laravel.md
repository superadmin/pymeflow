# PymeFlow: Implementación con Laravel

## Instalación con Composer
```sh
composer require sebacarrasco93/pymeflow
```

### Archivo .env
```env
FLOW_API_KEY=
FLOW_SECRET_KEY=
FLOW_API_URL=
FLOW_BASE_URL=
FLOW_DEBUG=
```

### Crear una orden
```php
public function create()
{
    $order = rand(1000, 2000);
    $subject = 'Pago de prueba';
    $optional = ['RUT' => '9999999-9', 'Otro dato' => 'Otro valor'];
    $amount = 5000;
    $email = 'prueba@algo.com';
    $paymentMethod = 9;

    PymeFlow::create($order, $subject, $amount, $email, $paymentMethod, $optional);
}
```

### Ejemplos para recibir la orden

Obtener el resultado desde flow (con detalle)
```php
public function confirm()
{
    $token = request()->post('token');

    // Actualizar la información en la DB, mostrar el Voucher propio, etc...
    // ...

    return PymeFlow::result($token);
}
```

O también sólo obtener el estado en formato de texto
```php
public function confirm()
{
    $token = request()->post('token');

    // Actualizar la información en la DB, mostrar el Voucher propio, etc...
    // ...

    $status = PymeFlow::status($token);
    return $status;
}
```

O incluso, saber si fue pagada (devuelve true o false)
```php
public function confirm()
{
    $token = request()->post('token');

    $isPaid = PymeFlow::isPaid($token);
    
    if ($isPaid) {
        // ...
    }
}
```

## Bonus: Trabajar con Base de Datos
La tabla ```pymeflow``` incluida en las migraciones del paquete, permite guardar el estado de las transacciones. Para guardarlo, sólo debes utilizar el método ```save```y pasarle el token como parámetro.

```php
public function confirm()
{
    $token = request()->post('token');

    // lógica...

    // y después guardar...
    PymeFlow::save($token);
}
```

Nota: Intenta usar siempre el método ```save```, para que después puedas sacar estadísticas valiosas (por ejemplo: cancelados/rechazados/anulados vs pagados)

### Scopes disponibles

| Nombre del scope | Descripción                      |
| ---------------- | -------------------------------- |
| paid             | Pagos pagados (sólo confirmados) |
| pending          | Pagos pendientes                 |
| canceled         | Pagos cancelados                 |
| rejected         | Pagos rechazados                 |

### Ver las órdenes guardadas en la DB
Útil para paneles de administración, conocer estadísticas, medir KPIs, etc.
```php
// Importar el modelo para utilizarlo de manera simplificada...
use SebaCarrasco93\PymeFlow\Models\Pymeflow as PymeflowDB;

$paid = PymeflowDB::paid()->get();
$pending = PymeflowDB::pending()->get();
$canceled = PymeflowDB::canceled()->get();
$rejected = PymeflowDB::rejected()->get();
```

### Obtener información
Como es Laravel, y es sólo un scope, se puede juntar con otros scopes e instrucciones propias de Laravel

#### Ejemplos:

```php
// Importar el modelo para utilizarlo de manera simplificada...
use SebaCarrasco93\PymeFlow\Models\Pymeflow as PymeflowDB;
```

```php
// Obtener todos los pagos de agosto de todos los años
$paid = PymeflowDB::paid()
    ->whereMonth('created_at', '=', '08')
    ->get();

return $paid; // regresa la colección completa
```

```php
// Obtener todos los rechazados de septiembre de 2020
$rejected = PymeflowDB::rejected()
    ->whereMonth('created_at', '=', '09')
    ->whereYear('created_at', '=', '2020')
    ->count();

return $rejected; // regresa la cuenta total, por ejemplo 5
```

```php
// Obtener todos los pagos cancelados del año 2020 y anteriores
$canceled = PymeflowDB::canceled()
    ->whereYear('created_at', '<=', '2020')
    ->count();

return $canceled; // regresa la cuenta total, por ejemplo 2
```

```php
// Obtener todos los pendientes sobre $50.000
$pending = PymeflowDB::pending()
    ->where('amount', '>', '50000')
    ->get();

return $pending; // regresa la colección con todos los pagos pendientes sobre el monto de 50000
```

```php
// Obtener todos los pagos de agosto de 2020, sin ningún scope de pagos
// Al no usar un scope, trae todos los estados: pagados, rechazados, cancelados y pendientes
$payments = PymeflowDB::whereMonth('created_at', '=', '08')
    ->whereYear('created_at', '=', '2020')
    ->get();

return $payments; // regresa la colección completa
```
