<?php

$base_route = config('pymeflow.base_url');
$namespace = 'SebaCarrasco93\PymeFlow\Http\Controllers';

Route::namespace($namespace)->prefix($base_route)->name('pymeflow.')->group(function(){
    Route::get('create', 'PymeFlowController@create')->name('create');
    Route::post('confirm', 'PymeFlowController@confirm')->name('confirm');
    Route::get('result', 'PymeFlowController@result')->name('result');
});
