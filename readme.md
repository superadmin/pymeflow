# PymeFlow

## Basado en el código original de Flow
```
https://github.com/flowcl/PHP-API-CLIENT
```

# Introducción
Ante la falta de una versión que fuera fácil de instalar y mantener con Composer, creé esta biblioteca que fue generada bajo el estándar PSR-4.
Es desacoplado (funciona sin Frameworks), pero también viene listo para usar con Laravel!

## Requerimientos
* PHP 7.2 o superior

# Otras bibliotecas usadas
* Guzzle (http://docs.guzzlephp.org/en/stable/overview.html)

## Instalación con Composer
```sh
composer require sebacarrasco93/pymeflow
```

## Aprende a instalar:
- [Con PHP Plano](./PHP-Plano.md)
- [Con Laravel](./Laravel.md)

### Descarga de responsabilidad
Este NO es un paquete oficial de Flow. No me hago responsable ante un mal funcionamiento

## Licencia
[MIT](./LICENSE.md)
