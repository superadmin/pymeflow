<?php

namespace SebaCarrasco93\PymeFlow;

use SebaCarrasco93\PymeFlow\Config;
use SebaCarrasco93\PymeFlow\FlowApi;
use SebaCarrasco93\PymeFlow\Traits\StatusByNumberTrait;

class Action
{
    use StatusByNumberTrait;

    public $params;
    public $serviceName;
    public $optional;
    public $method = 'GET';
    public $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
        $this->flowApi = new FlowApi($config);
    }

    public function create(string $commerceOrder, string $subject, int $amount, string $email, $paymentMethod, array $optional = null)
    {
        $this->serviceName = 'payment/create';
        $this->method = 'POST';

        $optional = json_encode($optional);

        $this->params = compact('commerceOrder', 'subject', 'amount', 'email', 'paymentMethod', 'optional');
        $this->params += $this->addUrlsToParams();
        $this->params['currency'] = 'CLP';

        return $this->execute(true);
    }

    public function confirm()
    {
        //
    }

    public function result(string $token)
    {
        $this->serviceName = 'payment/getStatus';

        $this->params = compact('token');

        return $this->execute();
    }

    public function status(string $token)
    {
        try {
            $statusNumber = $this->result($token)['status'];

            return $this->getStatusByNumber($statusNumber);
        } catch (\Exception $e) {
            echo "Sorry, we can't get the status from your Token: {$token}";
        }
    }

    public function isPaid(string $token)
    {
        return $this->status($token) == 'paid';
    }

    public function addUrlsToParams()
    {
        return [
            "urlConfirmation" => $this->config->get('baseUrl') . '/confirm',
            "urlReturn" => $this->config->get('baseUrl') . '/' //result,
        ];
    }

    public function execute($redirect = false)
    {
        try {
            $response = $this->flowApi->send($this->serviceName, $this->params, $this->method);

            if ($redirect) {
                $redirect = $response["url"] . "?token=" . $response["token"];
                header("location:$redirect");
            }

            return $response;
        } catch (\Exception $e) {
            echo 'Action.php -> execute: ' . $e->getCode() . " - " . $e->getMessage();
        }
    }
}
