<?php

namespace SebaCarrasco93\PymeFlow\Models;

use Illuminate\Database\Eloquent\Model;
use SebaCarrasco93\PymeFlow\Traits\StatusByNumberTrait;

class Pymeflow extends Model
{
    use StatusByNumberTrait;

    protected $table = 'pymeflow';
    protected $guarded = [];
    protected $casts = [
    	'optional' => 'array',
    	'pending_info' => 'array',
    	'payment_data' => 'array',
    	'merchant_id' => 'array',
    ];

    public function setStatusAttribute($status = null)
    {
        $this->attributes['status'] = $status;
        $this->attributes['status_text'] = $this->getStatusByNumber($status);
    }

    public function scopePending($q)
    {
        return $q->whereStatusText('pending');
    }

    public function scopePaid($q)
    {
        return $q->whereStatusText('paid');
    }

    public function scopeRejected($q)
    {
        return $q->whereStatusText('rejected');
    }

    public function scopeCanceled($q)
    {
        return $q->whereStatusText('canceled');
    }
}
