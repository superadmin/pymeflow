<?php

namespace SebaCarrasco93\PymeFlow;

use Illuminate\Support\ServiceProvider;

class PymeFlowServiceProvider extends ServiceProvider
{
    private $requiredEnvKeys = ['api_key', 'secret_key', 'api_url', 'base_url'];
    private $optionalEnvKeys = ['debug']; // TODO: Crear método para usar todos los opcionales!

    public function boot()
    {
        $this->loadRoutesFrom(
            $this->basePath('routes/web.php')
        );

        $this->loadMigrationsFrom(
            $this->basePath('/database/migrations')
        );

        $this->publishes([
            __DIR__ . '/../config/pymeflow.php' => base_path('config/pymeflow.php')
        ], 'pymeflow-config');
    }

    private function basePath($path)
    {
        return __DIR__ . '/../' . $path;
    }

    public function getEnvConfig()
    {
        foreach ($this->requiredEnvKeys as $requiredEnvKey) {
            $keys[$requiredEnvKey] = config('pymeflow.'.$requiredEnvKey);
        }

        if (count(array_filter($keys)) == count($this->requiredEnvKeys)) {
            return new Config(
                $keys['api_key'], $keys['secret_key'],
                $keys['api_url'], route('pymeflow.confirm'),
                config('pymeflow.debug', false)
            );
        }

        throw new \Exception(
            'No has agregado los campos necesarios en tu archivo .env. Son: [' . implode(', ', $this->requiredEnvKeys) . ']'
        );
    }

    public function register()
    {
        $this->app->bind('pyme-flow', function() {
            $config = $this->getEnvConfig();

            return new PymeFlow($config);
        });

        $this->mergeConfigFrom(__DIR__ . '/../config/pymeflow.php', 'pymeflow');
    }
}
