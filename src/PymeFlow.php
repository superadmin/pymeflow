<?php

namespace SebaCarrasco93\PymeFlow;

use SebaCarrasco93\PymeFlow\Models\Pymeflow as PymeFlowModel;

class PymeFlow
{
    protected $action;
    protected $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
        $this->action = new Action($config);
    }

    public function create(string $commerceOrder, string $subject, int $amount, string $email, $paymentMethod, array $optional = null)
    {
        $this->action->create($commerceOrder, $subject, $amount, $email, $paymentMethod, $optional);
    }

    public function result(string $token)
    {
        return $this->action->result($token);
    }

    public function status(string $token)
    {
        return $this->action->status($token);
    }

    public function isPaid(string $token)
    {
        return $this->action->isPaid($token);
    }

    public function save($token)
    {
        $result = $this->action->result($token);

        $data = [
            'token' => $token,
            'flow_order' => $result['flowOrder'],
            'commerce_order' => $result['commerceOrder'],
            'request_date' => $result['requestDate'],
            'status' => $result['status'],
            'subject' => $result['subject'],
            'currency' => $result['currency'],
            'amount' => $result['amount'],
            'payer' => $result['payer'],
            'optional' => $result['optional'],
            'pending_info' => $result['pending_info'],
            'payment_data' => $result['paymentData'],
            'merchant_id' => $result['merchantId'],
        ];

        // return $data['optional'];

        return PymeflowModel::updateOrCreate(['token' => $token], $data);
    }
}
