<?php

namespace SebaCarrasco93\PymeFlow\Traits;

trait StatusByNumberTrait
{
    public function getStatusByNumber($statusNumber)
    {
        switch ($statusNumber) {
            case 1:
                return 'pending'; // pendiente de pago
                break;
            case 2:
                return 'paid'; // pagada
                break;
            case 3:
                return 'rejected'; // rechazada
                break;
            case 4:
                return 'canceled'; // anulada
                break;
        }
    }
}
