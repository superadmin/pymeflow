<?php

namespace SebaCarrasco93\PymeFlow;

class Config
{
	private $apiKey;
	private $secretKey;
	private $apiUrl;
	private $baseUrl;
	private $debug;

	public function __construct(string $apiKey, string $secretKey, string $apiUrl, string $baseUrl, bool $debug = false)
	{
		$this->apiKey = $apiKey;
		$this->secretKey = $secretKey;
		$this->apiUrl = $apiUrl;
		$this->baseUrl = $baseUrl;
		$this->debug = $debug;
	}

	public function get($keyName) {
		if (!isset($this->$keyName)) {
			throw new \Exception("The configuration element thas not exist", 1);
		}
		
		return $this->$keyName;
	}
}
