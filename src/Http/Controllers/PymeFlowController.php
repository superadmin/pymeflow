<?php

namespace SebaCarrasco93\PymeFlow\Http\Controllers;

use PymeFlow;

class PymeFlowController
{
    public function create()
    {
        $order = rand(1000, 2000);
        $subject = 'Pago de prueba con Laravel PymeFlow';
        $optional = ['RUT' => '9999999-9', 'Otro dato' => 'Cualquier cosa'];
        $amount = 5000;
        $email = 'prueba@algo.com';
        $paymentMethod = 9;

        PymeFlow::create($order, $subject, $amount, $email, $paymentMethod, $optional);
    }
    
    public function confirm()
    {
        $token = request()->post('token');

        // Obtener resultado desde Flow (con detalle)
        return PymeFlow::result($token);

        // Actualizar la información en la DB, mostrar el Voucher propio, etc...
        // ...

        // O también sólo obtener el estado en formato de texto
        // $status = PymeFlow::status($token);
        // return $status;

        // O incluso, saber si fue pagada (devuelve true o false)
        // $isPaid = PymeFlow::isPaid($token);
        // return $isPaid;

        // Guardar en DB (sólo Laravel)
        // return PymeFlow::save($token);
    }

    public function result()
    {
        //
    }
}
