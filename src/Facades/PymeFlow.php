<?php

namespace SebaCarrasco93\PymeFlow\Facades;

use Illuminate\Support\Facades\Facade;

class PymeFlow extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'pyme-flow';
    }
}
