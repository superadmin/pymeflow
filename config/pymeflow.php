<?php

return [
    
    /* 
     * Configuraciones de PymeFlow
     */

    // Esto debes modificarlo desde tu archivo .env

	'api_key' => env('FLOW_API_KEY'),
	'secret_key' => env('FLOW_SECRET_KEY'),
	'api_url' => env('FLOW_API_URL'),
	'base_url' => env('FLOW_BASE_URL', 'pymeflow'),
	'debug' => env('FLOW_DEBUG'),

    /*
     * Si quieres cambiar la ruta base, puedes hacerlo aquí!
     * Sólo debes poner Lo que sigue de tu dominio, por ejemplo: [tudominio.com]/pymeflow.
     * Si es otro dominio, también puedes especificarlo
     */
//    'base_route' => 'pymeflow', // o 'pymeflow', o completo, así: https://tudominio.com/pymeflow
];
