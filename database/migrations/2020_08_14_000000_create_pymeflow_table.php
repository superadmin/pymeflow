<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePymeflowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pymeflow', function (Blueprint $table) {
            $table->id();
            $table->string('token', 100)->unique();
            $table->string('flow_order', 32);
            $table->string('commerce_order', 32);
            $table->datetime('request_date');
            $table->unsignedTinyInteger('status');
            $table->string('status_text', 15);
            $table->string('subject', 250)->nullable();
            $table->string('currency', 3);
            $table->unsignedInteger('amount');
            $table->string('payer', 32)->nullable();
            $table->json('optional')->nullable();
            $table->json('pending_info')->nullable();
            $table->json('payment_data')->nullable();
            $table->string('merchant_id', 32)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pymeflow');
    }
}
