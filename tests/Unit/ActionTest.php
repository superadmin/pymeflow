<?php

namespace SebaCarrasco93\PymeFlow\Tests\Unit;

// use PymeFlow;
use Illuminate\Support\Facades\Http;
use SebaCarrasco93\PymeFlow\Action;
use SebaCarrasco93\PymeFlow\Config;
use SebaCarrasco93\PymeFlow\Tests\TestCase;

class ActionTest extends TestCase
{
    protected function setUp() : void
    {
        parent::setUp();

        $this->url_api = 'https://sandbox.flow.cl/api';
        
        config(['pymeflow.api_url' => $this->url_api]);

        $this->config = new Config('api_key', 'api_url', $this->url_api, 'https://midominio.test/pymeflow');
        $this->action = new Action($this->config);
    }

    /** @test */
    public function can_add_urls_to_params() {
        $this->assertStringContainsString('/confirm', $this->action->addUrlsToParams()['urlConfirmation']);
        $this->assertStringContainsString('/result', $this->action->addUrlsToParams()['urlReturn']);
    }

    /** @test */
    public function create_method() {
        $order = rand(1000, 2000);
        $subject = 'Pago de prueba';
        $optional = ['rut' => '9999999-9', 'otroDato' => 'otroDato'];
        $amount = 5000;
        $email = 'prueba@algo.com';
        $paymentMethod = 9;

        $this->mockFakePayment();

        $response = Http::get($this->url_api);

//        dd($response->body());

        $create = $this->action->create($order, $subject, $amount, $email, $paymentMethod, $optional);

        dd($create);
    }

    public function mockFakePayment(): void
    {
        Http::fake([
            $this->url_api => Http::response([
                'flowOrder' => '1234',
                'commerceOrder' => '1234',
                'requestDate' => now(),
                'status' => 1,
                'subject' => 'Pago de prueba',
                'currency' => 'CLP',
                'amount' => '5000',
                'payer' => 'prueba@algo.com',
                'optional' => [
                    'RUT' => 'prueba@algo.com',
                    'Otro dato' => 'Cualquier cosa',
                ],
                'pending_info' => [
                    'media' => null,
                    'date' => null,
                ],
                'pending_info' => [
                    'date' => now(),
                    'media' => 'Media',
                    'conversionDate' => null,
                    'conversionRate' => null,
                    'amount' => '5000.00',
                    'currency' => 'CLP',
                    'fee' => '145.00',
                    'taxes' => 28,
                    'balance' => 4287,
                    'transferDate' => now(),
                ],
                'merchantId' => null,
            ], 200, ['Headers']),
        ]);
    }
}
