<?php

namespace SebaCarrasco93\PymeFlow\Tests\Unit;

use PymeFlow;
use SebaCarrasco93\PymeFlow\Tests\TestCase;

class FlowApiTest extends TestCase
{
    /** @test */
    public function muestra_el_mensaje_si_debug_esta_en_true()
    {
        config(['pymeflow.debug' => true]);

        $this->assertStringContainsString(
            'Se conectó a la API',
            PymeFlow::debug('Se conectó a la API')
        );
    }

    /** @test */
    public function no_muestra_el_mensaje_si_debug_esta_en_false()
    {
        config(['pymeflow.debug' => false]);

        $this->assertNull(
            PymeFlow::debug('Se conectó a la API')
        );
    }

    /** @test */
    public function metodo_send() {
        $this->markTestIncomplete('Falta terminar!');

        $optional = [
            "rut" => "9999999-9",
            "otroDato" => "otroDato"
        ];

        $params = [
            "commerceOrder" => rand(1100,2000),
            "subject" => "Pago de prueba con Test",
            "currency" => "CLP",
            "amount" => 5000,
            "email" => "prueba@gmail.com",
            "paymentMethod" => 9,
            "urlConfirmation" => config('pymeflow.base_url') . "/examples/payments/confirm.php",
            "urlReturn" => config('pymeflow.base_url') ."/examples/payments/result.php",
            "optional" => json_encode($optional)
        ];

        $serviceName = 'payment/create';

        dd(PymeFlow::send($serviceName, $params, 'POST'));
    }
}
