<?php

namespace SebaCarrasco93\PymeFlow\Tests\Feature;

use PymeFlow;
use SebaCarrasco93\PymeFlow\Tests\TestCase;

class RoutesTest extends TestCase
{
    /** @test */
    public function create() {
        $this->get(route('pymeflow.create'))->assertSuccessful();
    }

    /** @test */
    public function confirm() {
        $res = $this->post(route('pymeflow.confirm'), [
            'token' => 'un-token-inventado'
        ])->assertSuccessful();
    }

    /** @test */
    public function result() {
        $this->get(route('pymeflow.result'))->assertSuccessful();
    }
}
