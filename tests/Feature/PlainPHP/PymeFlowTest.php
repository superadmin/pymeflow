<?php

namespace SebaCarrasco93\PymeFlow\Tests\Feature\PlainPHP;

use SebaCarrasco93\PymeFlow\Config;
use SebaCarrasco93\PymeFlow\PymeFlow;
use SebaCarrasco93\PymeFlow\Tests\TestCase;

class PymeFlowTest extends TestCase
{
	/** @test */
	public function can_create_an_order() {
		$config = new Config('apiKey', 'secretKey', 'apiUrl', 'baseurl');
		$pymeflow = new PymeFlow($config);

		$order = rand(1000, 2000);
        $subject = 'Pago de prueba';
        $amount = 5000;
        $email = 'prueba@algo.com';
        $paymentMethod = 9;
        $optional = ['rut' => '9999999-9', 'otroDato' => 'otroDato'];

		$pymeflow->create($order, $subject, $amount, $email, $paymentMethod, $optional);
	}
}
