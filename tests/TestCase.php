<?php

namespace SebaCarrasco93\PymeFlow\Tests;

use SebaCarrasco93\PymeFlow\Facades\PymeFlow;
use SebaCarrasco93\PymeFlow\PymeFlowServiceProvider;

class TestCase extends \Orchestra\Testbench\TestCase
{
    protected function getPackageProviders($app)
    {
        return [
            PymeFlowServiceProvider::class
        ];
    }

    protected function getPackageAliases($app)
    {
        return [
            'PymeFlow' => PymeFlow::class
        ];
    }
}
